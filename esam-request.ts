import * as tools from './sme-tools';

class EsamRequest {
    cluster: string;
    clusterRoute: string;
    esam: string;
    servicesList: object;

    constructor(pClusterSpace: string, pCluster: string, pEsamHost: string) {
        this.cluster = pCluster;
        this.clusterRoute = pClusterSpace ? '/' + pClusterSpace + '/' + pCluster : '/' + pCluster;
        this.esam = pEsamHost + this.clusterRoute;

        // -- Fixed ESAm Operations Routes ----
        this.servicesList['ESAm'] = {
            'Host': pEsamHost,
            'Opers': {
                'get_service': {
                    'Route': 'get_service',
                    'method': 'GET'
                },
                'suscribe_service': {
                    'Route':'suscribe_service/',
                    'method':'POST'
                }
            }
        }
    }

    // -- Main operations ----
    method(pDomain, pService, pOperation) {
        try {
            let r;
            let operMethod = null;
            let serviceRoute = pService;

            if (pDomain !== null && pDomain !== '') {
                serviceRoute = pDomain + '/' + pService;
            }

            if (!(serviceRoute in this.servicesList)) {
                r = this.esamGetOpers(pDomain, pService);
                if (!tools.isOk(r)) {
                    return tools.returnErr('method', `failed to get opers of [${pService}]`, r);
                }
            } else {
                if (!(pOperation in this.servicesList[serviceRoute]['Opers'])) {
                    r = this.esamGetOpers(pDomain, pService);
                    if (!tools.isOk(r)) {
                        return tools.returnErr('method', `failed to get opers of [${pService}]`, r);
                    }
                }
            }

            if (serviceRoute in this.servicesList) {
                if (pOperation in this.servicesList[serviceRoute]['Opers']) {
                    operMethod = this.servicesList[serviceRoute]['Opers'][pOperation]['method'];
                }
            } else {
                return tools.returnErr('method', 'method not found');
            }

            return tools.returnOk(operMethod);
        } catch(e) {
            return tools.returnErr('method', `[EXCEPTION] ${JSON.stringify(e)}`);
        }
    }

    esamGetOpers(pDomain, pService) {
        let operUrl, parameters, r, result, route, serviceRoute;
        
        try {
            parameters = {};
            parameters['service'] = pService;
            serviceRoute = pService;
            result = null;
            if (pDomain !== null && pDomain !== '') {
                parameters['domain'] = pDomain;
                serviceRoute = pDomain + '/' + serviceRoute;
            }

            r = this.url(null, 'ESAm', 'get_service');
            if (!tools.isOk(r)) {
                return tools.returnErr('esamGetOpers', 'url of ESAm.get_service not found', r);
            }

            operUrl = tools.result(r);
            r = tools.restfulReq('GET', operUrl, null, parameters);
            if (!tools.isOk(r)) {
                return tools.returnErr('esamGetOpers', 'failed to consume ESAm.get_service', r);
            }

            result = tools.result(r);
            this.servicesList[serviceRoute] = {};
            this.servicesList[serviceRoute]['Host'] = result['Host'];
            this.servicesList[serviceRoute]['Opers'] = {};

            let op;
            for (op in result['Operations']) {
                if ('Subdomain' in op && op['Subdomain'] !== '') {
                    route = op['Subdomain'] + '/' + op['Code'];
                } else {
                    route = op['Code'];
                }
                this.servicesList[serviceRoute]['Opers'][op['Code']] = {'Route': route, 'method': op['Method']};
            }
            return tools.returnOk(result);
        } catch(e) {
            return tools.returnErr('consume', `[EXCEPTION] ${JSON.stringify(e)}`);
        }
    }

    url(pDomain, pService, pOperation) {
        let r, serviceRoute;

        try {
            let operUrl = null;

            if (pDomain !== null && pDomain !== '') {
                serviceRoute = pDomain + '/' + pService;
            } else {
                serviceRoute = pService;
            }

            if (!(serviceRoute in this.servicesList)) {
                r = this.esamGetOpers(pDomain, pService);
                if (!tools.isOk(r)) {
                    return tools.returnErr('url', `failed to get opers of [${pService}]`, r);
                }
            } else {
                if (!(pOperation in this.servicesList[serviceRoute]['Opers'])) {
                    r = this.esamGetOpers(pDomain, pService);
                    if (!tools.isOk(r)) {
                        return tools.returnErr('url', `failed to get opers of [${pService}]`, r);
                    }
                }
            }

            if (serviceRoute in this.servicesList) {
                if (pOperation in this.servicesList[serviceRoute]['Opers']) {
                    operUrl = `${this.servicesList[serviceRoute]['Host']}${this.clusterRoute}/
                                ${serviceRoute}/${this.servicesList[serviceRoute]['Opers'][pOperation]['Route']}`;
                }
            }
            return tools.returnOk(operUrl);
        } catch(e) {
            return tools.returnErr('url', `[EXCEPTION] ${JSON.stringify(e)}`);
        }
    }

    consume(pDomain, pService, pOperation, pData, parameters = null) {
        try {
            let r;

            r = this.url(pDomain, pService, pOperation);
            if (!tools.isOk(r)) {
                return tools.returnErr('consume', `url (domain[${pDomain}],service[${pService}],oper[${pOperation}]) not found`, r);
            }

            let operUrl = tools.result(r);
            r = this.method(pDomain, pService, pOperation);
            if (!tools.isOk(r)) {
                return tools.returnErr('consume', `method (domain[${pDomain}],service[${pService}],oper[${pOperation}]) not found`, r);
            }

            let operMethod = tools.result(r);
            if (operMethod === 'POST' && parameters === null) {
                operUrl += '/';
            }

            r = tools.restfulReq(operMethod, operUrl, pData, parameters);
            if (!tools.isOk(r)) {
                return tools.returnErr('consume', `failed to consume domain[${pDomain}] service[${pService}] oper[${pOperation}]`, r);
            }

            let result = tools.result(r);
            return tools.returnOk(result);
        } catch (e) {
            return tools.returnErr('consume', `[EXCEPTION] ${JSON.stringify(e)}`);
        }
    }
}