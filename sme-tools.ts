/* -- Third party imports -- */
import axios from 'axios';
import * as moment from 'moment';

export function isOk(pResult) {
    return ('Status' in pResult && pResult['Status'][0] === 'Ok') ? true : false;
}

export function returnErr(pFunc, pMessage, pContext = null) {
    let status = (pContext === null) ? [] : pContext['Status'].copy();
    status.append(`FUNCTION [${pFunc}] MESSAGE: ${pMessage}`);

    return {'Result': null, 'Status': status};
}

export function result(pResult) {
    return pResult['Result'];
}

export function esaIsOk(pResult) {
    return ('Status' in pResult && pResult['Status'] === 'Ok') ? true : false;
}

export function returnOk(pData) {
    return {'Result': pData, 'Status': ['Ok']};
}

export function esaData(pResult) {
    return pResult['Data'];
}

function esaCode(pResult) {
    return pResult['Status'];
}

export async function restfulReq(pMethod, pUrl, pData, parameters = null) {
    let result;

    try {
        switch (pMethod) {
            case 'GET':
                result = await axios.get(pUrl, {'params': parameters});
                break;
            case 'POST':
                result = (parameters === null) ? await axios.post(pUrl, {'json': pData}) : 
                    await axios.post(pUrl, {'json': pData, 'params': parameters});
                break;
            case 'PUT':
                result = await axios.put(pUrl, {'json': pData, 'params': parameters});
                break;
            case 'PATCH':
                result = await axios.patch(pUrl, {'json': pData, 'params': parameters});
                break;
            case 'DELETE':
                result = await axios.delete(pUrl, {'params': parameters});
                break;
            default:
                return returnErr('restfulReq', `INCORRECT REST METHOD [${pMethod}]`);  
        }

        let code = result.status_code;

        if (code === 200) {
            let r = result;

            if (esaIsOk(r)) {
                return returnOk(esaData(r));
            } else {
                return returnErr('restfulReq', `URL [${pUrl}]  ERROR CODE: [${esaCode(r)}]`);
            }
        } else {
            return returnErr('restfulReq', `URL [${pUrl}] [UNHANDLED ESA ERROR] [${code}]`);
        }
    } catch(e) {
        return returnErr('restfulReq', `[EXCEPTION] ${JSON.stringify(e)}`);
    }
}

export function dateToStr(pFecha, strFormat = 'DD/M/YYYY') {
    return pFecha.format(strFormat);
}

export function actualDate() {
    let today = moment();
    return today;
}

export function errMessage(pResult) {
    if ('Status' in pResult && pResult['Status'][0] !== 'Ok') {
        return pResult['Status'];
    } else {
        return null;
    }
}

export function debug(pResult, debugFlag, pFunc, pErrMessage) {
    if (debugFlag) {
        if (!isOk(pResult)) {
            let status = pResult['Status'].copy();
            status.append(`FUNCTION [${pFunc}] MESSAGE: ${pErrMessage}`);
            
            let fecha = dateToStr(actualDate(), 'DD/M/YYYY HH:mm:ss');
            console.log(`\n[${fecha}] INTERNAL ERROR ----> `);
            
            let n = 0;
            for (let i = 0; i < status.length; i++) {
                console.log(`\tLEVEL [${n}]: ${i}`);
                n++;
            }
            console.log('--------------------------------------------------------------------------\n');
        }
    }
    return pResult;
}

export function debugResult(pMessage, pResult) {
    if (isOk(pResult)) {
        console.log(pMessage, result(pResult));
    } else {
        let n = 0;
        console.log('** ERROR:');
        for (let i = 0; i < errMessage(pResult); i++) {
            console.log(`\tnivel [${n}]: ${i}`);
            n++;
        }
    }
}